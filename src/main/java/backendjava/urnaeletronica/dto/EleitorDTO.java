package backendjava.urnaeletronica.dto;

import java.util.List;
import java.util.stream.Collectors;

import backendjava.urnaeletronica.model.Eleitor;

public class EleitorDTO {

	private String cpf;

	public EleitorDTO(Eleitor eleitor) {
		super();
		this.cpf = eleitor.getCpf();
	}

	public String getCpf() {
		return cpf;
	}

	public static List<EleitorDTO> converter(List<Eleitor> eleitores) {
		return eleitores.stream().map(EleitorDTO::new).collect(Collectors.toList());
	}

	public static EleitorDTO converter(Eleitor eleitor) {
		return new EleitorDTO(eleitor);
	}

}
