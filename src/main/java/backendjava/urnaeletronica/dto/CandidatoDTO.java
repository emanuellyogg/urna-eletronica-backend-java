package backendjava.urnaeletronica.dto;

import java.util.List;
import java.util.stream.Collectors;

import backendjava.urnaeletronica.model.Candidato;

public class CandidatoDTO {
	private int numero;
	private String nome;
	private String foto;
	private String descricao;
	
	public CandidatoDTO(Candidato candidato) {
		this.numero = candidato.getNumero();
		this.nome = candidato.getNome();
		this.foto = candidato.getFoto();
		this.descricao = candidato.getDescricao();
	}
	
	public int getNumero() {
		return numero;
	}
	public String getNome() {
		return nome;
	}
	public String getFoto() {
		return foto;
	}
	public String getDescricao() {
		return descricao;
	}
	
	public static List<CandidatoDTO> converter(List<Candidato> candidatos) {
		return candidatos.stream().map(CandidatoDTO::new).collect(Collectors.toList());
	}
	
	public static CandidatoDTO converter(Candidato candidato) {
		return new CandidatoDTO(candidato);
	}
}
