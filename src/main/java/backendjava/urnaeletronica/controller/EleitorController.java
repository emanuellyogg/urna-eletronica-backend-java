package backendjava.urnaeletronica.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import backendjava.urnaeletronica.dto.EleitorDTO;
import backendjava.urnaeletronica.form.AtualizacaoEleitorForm;
import backendjava.urnaeletronica.form.EleitorForm;
import backendjava.urnaeletronica.model.Eleitor;
import backendjava.urnaeletronica.repository.EleitorRepository;

@RestController
@RequestMapping("/eleitor")
public class EleitorController {

	@Autowired
	private EleitorRepository eleitorRepository;

	@GetMapping
	public List<EleitorDTO> listarEleitores() {

		List<Eleitor> eleitores = eleitorRepository.findAll();

		return EleitorDTO.converter(eleitores);
	}

	@GetMapping("/{cpf}")
	public EleitorDTO buscarEleitor(@PathVariable String cpf) {

		Optional<Eleitor> eleitorOp = eleitorRepository.findByCpf(cpf);

		return EleitorDTO.converter(eleitorOp.get());
	}
	
	@PostMapping
	@Transactional 
	public ResponseEntity<EleitorDTO>salvar(@RequestBody EleitorForm eleitorForm, UriComponentsBuilder uriBuilder){
		Eleitor eleitor = eleitorForm.converter();
		eleitorRepository.save(eleitor);
		URI uri = uriBuilder.path("/eleitor/{id}").buildAndExpand(eleitor.getId()).toUri();
		
		return ResponseEntity.created(uri).body(new EleitorDTO(eleitor));
	}
	
	@PutMapping("/{cpf}")
	@Transactional
	public ResponseEntity<EleitorDTO>atualizarEleitor(@PathVariable String cpf, @RequestBody AtualizacaoEleitorForm eleitorForm) {
		Eleitor eleitor = eleitorForm.atualizar(cpf, eleitorRepository);
		if (eleitor != null) {
			return ResponseEntity.ok(new EleitorDTO(eleitor));
			
		}
		return ResponseEntity.notFound().build();
	}
	
	@DeleteMapping("/{cpf}")
	@Transactional
	public ResponseEntity<?>deletarEleitor(@PathVariable String cpf){
		Optional<Eleitor>eleitor = eleitorRepository.findByCpf(cpf);
		if (eleitor.isPresent()) {
			eleitorRepository.deleteByCpf(cpf);
			return ResponseEntity.ok().build();
			
		}
		return ResponseEntity.notFound().build();
		
	}
	
	
	
	

}
