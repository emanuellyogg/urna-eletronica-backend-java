package backendjava.urnaeletronica.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import backendjava.urnaeletronica.dto.CandidatoDTO;
import backendjava.urnaeletronica.form.AtualizacaoCandidatoForm;
import backendjava.urnaeletronica.form.CandidatoForm;
import backendjava.urnaeletronica.model.Candidato;
import backendjava.urnaeletronica.repository.CandidatoRepository;

@RestController
@RequestMapping("/candidato")
public class CandidatoController {
	
	@Autowired
	private CandidatoRepository candidatoRepository;
	
	@CrossOrigin
	@GetMapping
	public List<CandidatoDTO> listarCandidatos() {
		List<Candidato> candidatos = candidatoRepository.findAll();
		return CandidatoDTO.converter(candidatos);
	}
	
	@CrossOrigin
	@GetMapping("/{numero}")
	public ResponseEntity<CandidatoDTO> buscarCandidato(@PathVariable int numero) {
		Optional<Candidato> candidatoOp = candidatoRepository.findByNumero(numero);
		if (candidatoOp.isPresent()) {
			return ResponseEntity.ok(CandidatoDTO.converter(candidatoOp.get()));
		}
		return ResponseEntity.notFound().build();
	}
	
	@CrossOrigin
	@PostMapping
	@Transactional 
	public ResponseEntity<CandidatoDTO>salvar(@RequestBody CandidatoForm candidatoForm, UriComponentsBuilder uriBuilder){
		Candidato candidato = candidatoForm.converter();
		candidatoRepository.save(candidato);
		URI uri = uriBuilder.path("/candidato/{id}").buildAndExpand(candidato.getId()).toUri();
		return ResponseEntity.created(uri).body(new CandidatoDTO(candidato));
	}
	
	@CrossOrigin
	@PutMapping("/{numero}")
	@Transactional
	public ResponseEntity<CandidatoDTO>atualizarCandidato(@PathVariable int numero, @RequestBody AtualizacaoCandidatoForm candidatoForm) {
		Candidato candidato = candidatoForm.atualizar(numero, candidatoRepository);
		if (candidato != null) {
			return ResponseEntity.ok(new CandidatoDTO(candidato));
		}
		return ResponseEntity.notFound().build();
	}
	
	@CrossOrigin
	@DeleteMapping("/{numero}")
	@Transactional
	public ResponseEntity<?>deletarEleitor(@PathVariable int numero) {
		Optional<Candidato> candidato = candidatoRepository.findByNumero(numero);
		if (candidato.isPresent()) {
			candidatoRepository.deleteByNumero(numero);
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();
	}
}
