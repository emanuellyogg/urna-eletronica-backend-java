package backendjava.urnaeletronica.form;

import java.util.Optional;

import backendjava.urnaeletronica.model.Candidato;
import backendjava.urnaeletronica.repository.CandidatoRepository;


public class AtualizacaoCandidatoForm {
	private int numero;
	private String nome;
	private String foto;
	private String descricao;
	
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public Candidato atualizar(int numero, CandidatoRepository candidatoRepository) {
		Optional<Candidato> candidato = candidatoRepository.findByNumero(numero);
		if (candidato.isPresent()) {
			candidato.get().setNumero(this.numero);
			candidato.get().setNome(this.nome);
			candidato.get().setFoto(this.foto);
			candidato.get().setDescricao(this.descricao);			
			return candidato.get();
		}
		return null;
	}
	
}
