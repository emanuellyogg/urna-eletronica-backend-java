package backendjava.urnaeletronica.form;

import java.util.Optional;

import backendjava.urnaeletronica.model.Eleitor;
import backendjava.urnaeletronica.repository.EleitorRepository;

public class AtualizacaoEleitorForm {
	
	private String cpf;
	
	

	public String getCpf() {
		return cpf;
	}



	public void setCpf(String cpf) {
		this.cpf = cpf;
	}



	public Eleitor atualizar(String cpf, EleitorRepository eleitorRepository) {
		
		Optional<Eleitor>eleitor = eleitorRepository.findByCpf(cpf);
		if (eleitor.isPresent()) {
			eleitor.get().setCpf(this.cpf);
			return eleitor.get();
			
		}
		return null;
	}

}
