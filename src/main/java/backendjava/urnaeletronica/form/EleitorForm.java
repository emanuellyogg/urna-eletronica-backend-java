package backendjava.urnaeletronica.form;

import backendjava.urnaeletronica.model.Eleitor;

public class EleitorForm {
	
	private int id;
	private String cpf;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public Eleitor converter() {
		
		return new Eleitor(cpf, id);
	}
	
	

}
