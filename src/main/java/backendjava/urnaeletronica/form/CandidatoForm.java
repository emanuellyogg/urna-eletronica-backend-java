package backendjava.urnaeletronica.form;

import backendjava.urnaeletronica.model.Candidato;

public class CandidatoForm {
	private int id;
	private int numero;
	private String nome;
	private String foto;
	private String descricao;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Candidato converter() {
		return new Candidato(id, numero, nome, foto, descricao);
	}

}
