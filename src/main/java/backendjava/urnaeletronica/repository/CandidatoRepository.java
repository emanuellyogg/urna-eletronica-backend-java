package backendjava.urnaeletronica.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import backendjava.urnaeletronica.model.Candidato;

public interface CandidatoRepository extends JpaRepository<Candidato, Integer> {

	Optional<Candidato> findByNumero(int numero);

	void deleteByNumero(int numero);

}
