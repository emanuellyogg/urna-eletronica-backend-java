package backendjava.urnaeletronica.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import backendjava.urnaeletronica.model.Eleitor;

public interface EleitorRepository extends JpaRepository<Eleitor, Integer> {

	Optional<Eleitor> findByCpf(String cpf);

	void deleteByCpf(String cpf);
	
	

}
