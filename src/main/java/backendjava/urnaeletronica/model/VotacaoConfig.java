package backendjava.urnaeletronica.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

//@Entity
public class VotacaoConfig {

	//@Id
	private int configId;
	private String tipo;
	private Date inicioVotacao;
	private Date finalVotacao;

	public VotacaoConfig(int configId, String tipo, Date inicioVotacao, Date finalVotacao) {
		super();
		this.configId = configId;
		this.tipo = tipo;
		this.inicioVotacao = inicioVotacao;
		this.finalVotacao = finalVotacao;
	}

	public int getConfigId() {
		return configId;
	}

	public void setConfigId(int configId) {
		this.configId = configId;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Date getInicioVotacao() {
		return inicioVotacao;
	}

	public void setInicioVotacao(Date inicioVotacao) {
		this.inicioVotacao = inicioVotacao;
	}

	public Date getFinalVotacao() {
		return finalVotacao;
	}

	public void setFinalVotacao(Date finalVotacao) {
		this.finalVotacao = finalVotacao;
	}

//	// MÉTODOS
//	public List<Candidato> criarVetorCandidato() {
//		//Lógica para listar os candidatos...
//		return null;
//	}

}
