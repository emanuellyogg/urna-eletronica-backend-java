package backendjava.urnaeletronica.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

//@Entity
public class VotacaoCandidato {
	
	//@ManyToOne @JoinColumn(name="votacao_id")
	private VotacaoConfig id_votacao;
	
	//@ManyToOne @JoinColumn(name="candidato_id")
	private Candidato id_candidato;
	
	public VotacaoCandidato() {
		
	}

	public VotacaoCandidato(VotacaoConfig id_votacao, Candidato id_candidato) {
		this.id_votacao = id_votacao;
		this.id_candidato = id_candidato;
	}

	public VotacaoConfig getId_votacao() {
		return id_votacao;
	}

	public void setId_votacao(VotacaoConfig id_votacao) {
		this.id_votacao = id_votacao;
	}

	public Candidato getId_candidato() {
		return id_candidato;
	}

	public void setId_candidato(Candidato id_candidato) {
		this.id_candidato = id_candidato;
	}

}
