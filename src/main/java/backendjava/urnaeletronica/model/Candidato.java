package backendjava.urnaeletronica.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity 
public class Candidato {
	
	@Id
	private int id;
	private int numero;
	private String nome;
	private String foto;
	private String descricao;
	
	public Candidato() {
		
	}
	public Candidato(int id, int numero, String nome, String foto, String descricao) {
		super();
		this.id = id;
		this.numero = numero;
		this.nome = nome;
		this.foto = foto;
		this.descricao = descricao;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
