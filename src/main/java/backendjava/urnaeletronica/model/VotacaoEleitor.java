package backendjava.urnaeletronica.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

//@Entity
public class VotacaoEleitor {
	
	//@ManyToOne @JoinColumn(name="votacao_id")
	private VotacaoConfig id_votacao;
	
	//@ManyToOne @JoinColumn(name="eleitor_id")
	private Eleitor id_eleitor;
	
	public VotacaoEleitor() {
		
	}

	public VotacaoEleitor(VotacaoConfig id_votacao, Eleitor id_eleitor) {
		this.id_votacao = id_votacao;
		this.id_eleitor = id_eleitor;
	}

	public VotacaoConfig getId_votacao() {
		return id_votacao;
	}

	public void setId_votacao(VotacaoConfig id_votacao) {
		this.id_votacao = id_votacao;
	}

	public Eleitor getId_eleitor() {
		return id_eleitor;
	}

	public void setId_eleitor(Eleitor id_eleitor) {
		this.id_eleitor = id_eleitor;
	}
	
	
}
