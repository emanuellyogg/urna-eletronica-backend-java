package backendjava.urnaeletronica.model;

import java.util.Date;

import javax.persistence.*;

//@Entity
public class Voto {
	//@Id
	private int id;
	private Date dataVoto;
	
	//@ManyToOne @JoinColumn(name="eleitor_id")
	private Eleitor id_eleitor;
	
	//@ManyToOne @JoinColumn(name="votacao_id")
	private VotacaoConfig id_votacao;
	
	//@ManyToOne @JoinColumn(name="candidato_id")
	private Candidato id_candidato;
	
	public Voto() {}

	public Voto(int id, Date dataVoto, Eleitor id_eleitor, VotacaoConfig id_votacao, Candidato id_candidato) {
		super();
		this.id = id;
		this.dataVoto = dataVoto;
		this.id_eleitor = id_eleitor;
		this.id_votacao = id_votacao;
		this.id_candidato = id_candidato;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getDataVoto() {
		return dataVoto;
	}
	public void setDataVoto(Date dataVoto) {
		this.dataVoto = dataVoto;
	}
	public Eleitor getId_eleitor() {
		return id_eleitor;
	}
	public void setId_eleitor(Eleitor id_eleitor) {
		this.id_eleitor = id_eleitor;
	}
	public VotacaoConfig getId_votacao() {
		return id_votacao;
	}
	public void setId_votacao(VotacaoConfig id_votacao) {
		this.id_votacao = id_votacao;
	}
	public Candidato getId_candidato() {
		return id_candidato;
	}
	public void setId_candidato(Candidato id_candidato) {
		this.id_candidato = id_candidato;
	}
}
