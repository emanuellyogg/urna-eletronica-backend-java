package backendjava.urnaeletronica.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Eleitor {
	private String cpf;
	
	@Id
	private int id;
		
	public Eleitor(String cpf, int id) {
		super();
		this.cpf = cpf;
		this.id = id;
	}
	
	public Eleitor() {
		
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
